from datetime import datetime
import pytz

from django.test import TestCase
from django.contrib.auth.models import User

from .models import (UserSleepData, UserStepsData, UserGeoData)


class GetUserSleepData(TestCase):
    def setUp(self):
        user1 = User.objects.create_user('test_user1', email='1@mail.ru',
                                         password='89f8ad9asd')
        user2 = User.objects.create_user('test_user2', email='2@mail.ru',
                                         password='89f8ad9asd')

        UserSleepData.objects.create(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 3, tzinfo=pytz.UTC)
        )
        UserSleepData.objects.create(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 5, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 8, tzinfo=pytz.UTC)
        )
        UserSleepData.objects.create(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 10, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 13, tzinfo=pytz.UTC)
        )

        UserSleepData.objects.create(
            user=user2,
            time_start=datetime(2017, 6, 20, 12, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 12, 3, tzinfo=pytz.UTC)
        )
        UserSleepData.objects.create(
            user=user2,
            time_start=datetime(2017, 6, 20, 12, 5, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 12, 8, tzinfo=pytz.UTC)
        )
        UserSleepData.objects.create(
            user=user2,
            time_start=datetime(2017, 6, 20, 12, 10, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 12, 13, tzinfo=pytz.UTC)
        )

    def test_check_sleeps_for_users(self):
        user1 = User.objects.get(email='1@mail.ru')
        user2 = User.objects.get(email='2@mail.ru')

        self.assertEqual(UserSleepData.objects.get_user_data_inbetween(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 13, tzinfo=pytz.UTC)
        ).count(), 3)

        self.assertEqual(UserSleepData.objects.get_user_data_inbetween(
            user=user2,
            time_start=datetime(2017, 6, 20, 11, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 13, tzinfo=pytz.UTC)
        ).count(), 0)

        self.assertEqual(UserSleepData.objects.get_user_data_inbetween(
            user=user1,
            time_start=datetime(2017, 6, 20, 12, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 12, 13, tzinfo=pytz.UTC)
        ).count(), 0)

        self.assertEqual(UserSleepData.objects.get_user_data_inbetween(
            user=user2,
            time_start=datetime(2017, 6, 20, 12, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 12, 13, tzinfo=pytz.UTC)
        ).count(), 3)

        self.assertEqual(UserSleepData.objects.get_user_data_inbetween(
            user=None,
            time_start=None,
            time_end=None
        ).count(), 0)


class GetUserStepsData(TestCase):
    def setUp(self):
        user1 = User.objects.create_user('test_user1', email='1@mail.ru',
                                 password='89f8ad9asd')
        user2 = User.objects.create_user('test_user2', email='2@mail.ru',
                                 password='89f8ad9asd')

        UserStepsData.objects.create(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 3, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 5, tzinfo=pytz.UTC), steps=3
        )
        UserStepsData.objects.create(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 8, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 10, tzinfo=pytz.UTC), steps=14
        )
        UserStepsData.objects.create(
            user=user2,
            time_start=datetime(2017, 6, 20, 12, 3, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 12, 5, tzinfo=pytz.UTC), steps=3
        )

    def test_check_steps_for_users(self):
        user1 = User.objects.get(email='1@mail.ru')
        user2 = User.objects.get(email='2@mail.ru')

        self.assertEqual(UserStepsData.objects.get_user_data_inbetween(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 13, tzinfo=pytz.UTC)
        ).count(), 2)

        self.assertEqual(UserStepsData.objects.get_user_data_inbetween(
            user=user2,
            time_start=datetime(2017, 6, 20, 12, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 12, 13, tzinfo=pytz.UTC)
        ).count(), 1)

        self.assertEqual(UserStepsData.objects.get_user_data_inbetween(
            user=None,
            time_start=None,
            time_end=None
        ).count(), 0)


class GetUserGeoData(TestCase):
    def setUp(self):
        user1 = User.objects.create_user('test_user1', email='1@mail.ru',
                                 password='89f8ad9asd')
        user2 = User.objects.create_user('test_user2', email='2@mail.ru',
                                 password='89f8ad9asd')
        UserGeoData.objects.create(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 8, tzinfo=pytz.UTC),
            latitude=50,
            longitude=40
        )
        UserGeoData.objects.create(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 10, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 13, tzinfo=pytz.UTC),
            latitude=51,
            longitude=44
        )
        UserGeoData.objects.create(
            user=user2,
            time_start=datetime(2017, 6, 20, 12, 10, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 12, 13, tzinfo=pytz.UTC),
            latitude=51,
            longitude=44
        )

    def test_check_geo_for_users(self):
        user1 = User.objects.get(email='1@mail.ru')
        user2 = User.objects.get(email='2@mail.ru')

        self.assertEqual(UserGeoData.objects.get_user_data_inbetween(
            user=user1,
            time_start=datetime(2017, 6, 20, 11, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 11, 13, tzinfo=pytz.UTC)
        ).count(), 2)

        self.assertEqual(UserGeoData.objects.get_user_data_inbetween(
            user=user2,
            time_start=datetime(2017, 6, 20, 12, 0, tzinfo=pytz.UTC),
            time_end=datetime(2017, 6, 20, 12, 13, tzinfo=pytz.UTC)
        ).count(), 1)

        self.assertEqual(UserGeoData.objects.get_user_data_inbetween(
            user=None,
            time_start=None,
            time_end=None
        ).count(), 0)
