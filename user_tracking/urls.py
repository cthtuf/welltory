from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from .views import (UserSleepsViewSet, UserStepsViewSet, UserGeoViewSet)


router = DefaultRouter()
router.register(r'sleeps', UserSleepsViewSet, base_name='sleeps')
router.register(r'steps', UserStepsViewSet, base_name='steps')
router.register(r'geos', UserGeoViewSet, base_name='geos')


urlpatterns = [
    url(r'^', include(router.urls)),
]
