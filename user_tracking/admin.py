from django.contrib import admin

from .models import (UserSleepData, UserStepsData, UserGeoData)


admin.site.register(UserSleepData)
admin.site.register(UserStepsData)
admin.site.register(UserGeoData)
