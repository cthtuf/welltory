from rest_framework import serializers

from .models import (UserSleepData, UserStepsData, UserGeoData)


class UserSleepSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserSleepData
        fields = ('id', 'time_start', 'time_end',)


class UserStepsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserStepsData
        fields = ('id', 'time_start', 'time_end', 'steps')


class UserGeoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserGeoData
        fields = ('id', 'time_start', 'time_end', 'latitude', 'longitude',)
