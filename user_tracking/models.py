from django.db import models


class UserDataManager(models.Manager):
    """
    Менеджер, реализующий метод получения данных пользователя
    """
    def get_user_data_inbetween(self, user, time_start, time_end):
        """
        Метод получения данных пользователя в указанный промежуток времени
        Args:
            user: Запрашиваемый пользователь
            time_start: Время начала
            time_end: Время окончания

        Returns:
            Набор данных (queryset) отфильтрованный по заданным параметрам
        """
        if None in (user, time_start, time_end):
            return super(UserDataManager, self).none()

        return super(UserDataManager, self).get_queryset().filter(
            user=user,
            time_start__gte=time_start,
            time_start__lte=time_end
        )


class UserData(models.Model):
    """
    Абстрактная модель, описывающая общие для всех моделей поля:
    - Пользователь
    - Дата\время начала
    - Дата\время окончания
    """
    objects = UserDataManager()
    time_start = models.DateTimeField()
    time_end = models.DateTimeField()

    class Meta:
        abstract = True


class UserSleepData(UserData):
    """
    Промежутки сна
    """
    user = models.ForeignKey('auth.User', related_name='user_sleep_data',
                             on_delete=models.CASCADE)

    def __str__(self):
        return "From %(from)s to %(to)s" % (
            {'from': self.time_start, 'to': self.time_end}
        )


class UserStepsData(UserData):
    """
    Шаги, за промежуток времени
    """
    user = models.ForeignKey('auth.User', related_name='user_steps_data',
                             on_delete=models.CASCADE)
    steps = models.IntegerField(default=0)

    def __str__(self):
        return "From %(from)s to %(to)s, Steps: %(steps)s" % (
            {'from': self.time_start, 'to': self.time_end, 'steps': self.steps}
        )


class UserGeoData(UserData):
    """
    Присутствие пользователя в гео-точке
    """
    user = models.ForeignKey('auth.User', related_name='user_geo_data',
                             on_delete=models.CASCADE)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)

    def __str__(self):
        return "From %(from)s to %(to)s, Latitude: %(la)s, Longitude: %(lo)s" % (
            {'from': self.time_start, 'to': self.time_end,
             'la': self.latitude, 'lo': self.longitude}
        )
