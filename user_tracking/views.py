from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import (api_view, authentication_classes,
                                       permission_classes)
from rest_framework.exceptions import ParseError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .models import (UserSleepData, UserStepsData, UserGeoData)
from .serializers import (UserSleepSerializer, UserStepsSerializer,
                          UserGeoSerializer)


@api_view(['GET'])
@authentication_classes(SessionAuthentication, )
@permission_classes(IsAuthenticated, )
def api_root(request, format=None):
    return Response({
        'sleeps': reverse('sleeps-list', request=request, format=format),
        'steps': reverse('steps-list', request=request, format=format),
        'geos': reverse('geos-list', request=request, format=format)
    })


class UserDataViewSet(viewsets.ModelViewSet):
    authentication_classes = SessionAuthentication,
    permission_classes = IsAuthenticated,
    queryset_class = None
    queryset = None

    def list(self, request, *args, **kwargs):
        if not all(k in request.GET for k in ('time_start', 'time_end')):
            raise ParseError(
                'Required parameters (time_start, time_end) has not provided')
        queryset = self.queryset_class.objects.get_user_data_inbetween(
                user=request.user,
                time_start=request.GET['time_start'],
                time_end=request.GET['time_end']
            ).all()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class UserSleepsViewSet(UserDataViewSet):
    serializer_class = UserSleepSerializer
    queryset = UserSleepData.objects.all()
    queryset_class = UserSleepData


class UserStepsViewSet(UserDataViewSet):
    serializer_class = UserStepsSerializer
    queryset = UserStepsData.objects.all()
    queryset_class = UserStepsData


class UserGeoViewSet(UserDataViewSet):
    serializer_class = UserGeoSerializer
    queryset = UserGeoData.objects.all()
    queryset_class = UserGeoData
